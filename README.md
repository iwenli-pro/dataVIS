<!-- <p align="center">
    <img width="200" src="https://cdn.zhoukaiwen.com/logo.png">
</p> -->
<p align="center">
	<h1 align="center">大数据可视化电子沙盘</h1>
</p>

</br></br>

<p align="center">
	<a href="https://gitee.com/kevin_chou/dataVIS/stargazers" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=star&user=kevin_chou&project=dataVIS"/>
	</a>
	<a href="https://gitee.com/kevin_chou/dataVIS/members" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=fork&user=kevin_chou&project=dataVIS"/>
	</a>
	<img src="https://svg.hamm.cn/badge.svg?key=Platform&value=Pc端"/>
</p>



<div align="center">



<p>使用HTML、CSS、JavaScript，实现的可视化大数据电子沙盘</p>

```
如果觉得对你有用，随手点个 🌟 Star 🌟 支持下，这样才有持续下去的动力，谢谢！～
```

</div>

</br></br>
### 体验地址（请全屏查看）：
> *  智慧社区：[https://zhoukaiwen.com/proj/dataVIS/community](https://zhoukaiwen.com/proj/dataVIS/community/index.html)
> *  金融行业：[https://zhoukaiwen.com/proj/dataVIS/finance](https://zhoukaiwen.com/proj/dataVIS/finance/index.html)

### 项目说明

1.  项目基于html/css/js，包含行业：
> * 智慧政务 
> * 智慧社区
> * 金融行业
> * 智慧交通
> * 大数据分析平台

</br></br>

2.  项目包含功能 (部分)：
> * 实时数据K线图（可自由配置多种行业模式）
> * 可切换式大屏展示
> * 翻牌效果
> * 自定义字体
> * 更多功能还在更新中...

> 
3.  分享模版是为了大家做相关需求时方便查阅参考～
4.  其他页面还在补充中，开源项目中接口均已删除～
5.  开源目的就是为了方便同行的小伙伴，还请各位多多Star支持～ 
6.  本人自知技术还处于底层，还是有很多需要跟大家学习的地方！


### 部分截图
<img src="https://cdn.zhoukaiwen.com/dataVIS3.png" width="100%" />
<img src="https://cdn.zhoukaiwen.com/dataVIS1.jpeg" width="100%" />
<img src="https://cdn.zhoukaiwen.com/dataVIS2.jpeg" width="100%" />
<img src="https://cdn.zhoukaiwen.com/dataVIS4.jpeg" width="100%" />
<img src="https://cdn.zhoukaiwen.com/dataVIS5.jpeg" width="100%" />



### 项目信息

1.  作者：周凯文 (Kevin)
2.  邮箱：280224091@qq.com
3.  微信：280224091



### 其他说明
> *  作者 首页 [www.zhoukaiwen.com](https://www.zhoukaiwen.com)（维护中...）
> *  项目功能需协助，请联系作者微信：280224091



### 赞助作者
> *  打赏就duck不必啦～ 就点点🌟 Star 🌟 关注更新，支持下作者就可以了
<!-- <img src="https://cdn.zhoukaiwen.com/fk_zfb.jpeg" width="200"/>
<img src="https://cdn.zhoukaiwen.com/fk_wx.jpeg" width="200" /> -->


### 如果有项目需求、有设计，可以联系我～